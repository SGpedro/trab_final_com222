var Checkin = require("../models/checkin");
var Comment = require("../models/comment");

// todos os middlewares vao aqui
var middlewareObj = {};

middlewareObj.checkCheckinOwnership = function(req, res, next) {
 if(req.isAuthenticated()){
        Checkin.findById(req.params.id, function(err, foundCheckin){
           if(err){
               req.flash("error", "checkin não encontrado.");
               res.redirect("back");
           }  else {
               // o usuario tem um checkin?
            if(foundCheckin.author.id.equals(req.user._id) || req.user.isAdmin) {
                next();
            } else {
                req.flash("error", "Você nao tem permissão.");
                res.redirect("back");
            }
           }
        });
    } else {
        req.flash("error", "Você precisa estar logado para continuar.");
        res.redirect("back");
    }
}

middlewareObj.checkCommentOwnership = function(req, res, next) {
 if(req.isAuthenticated()){
        Comment.findById(req.params.comment_id, function(err, foundComment){
           if(err){
               res.redirect("back");
           }  else {
               // o usuario tem um comentario?
            if(foundComment.author.id.equals(req.user._id) || req.user.isAdmin) {
                next();
            } else {
                req.flash("error", "Você não tem permissão.");
                res.redirect("back");
            }
           }
        });
    } else {
        req.flash("error", "Você precisa estar logado para continuar.");
        res.redirect("back");
    }
}

middlewareObj.isLoggedIn = function(req, res, next){
    if(req.isAuthenticated()){
        return next();
    }
    req.flash("error", "Você precisa estar logado para continuar.");
    res.redirect("/login");
}

module.exports = middlewareObj;