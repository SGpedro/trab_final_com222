var express = require("express");
var router  = express.Router();
var passport = require("passport");
var User = require("../models/user");
var Checkin = require("../models/checkin");

//root route
router.get("/", function(req, res){
    res.render("landings");
});

// mostra formulario de cadastro
router.get("/register", function(req, res){
   res.render("register", {page: 'register'}); 
});

//mosdtra formulario de login
router.get("/login", function(req, res){
   res.render("login", {page: 'login'}); 
});

//logica para se cadastrar
router.post("/register", function(req, res){
    var newUser = new User(
        {
            username: req.body.username,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            avatar: req.body.avatar
        });
    if(req.body.adminCode === 'secretcode123' ){
        newUser.isAdmin = true;
    }
    User.register(newUser, req.body.password, function(err, user){
        if(err){
            return res.render("register", {"error": err.message});
        }
        passport.authenticate("local")(req, res, function(){
           req.flash("success", "Bem vindo ao Untapd " + user.username);
           res.redirect("/checkin"); 
        });
    });
});



//logica para se logar
router.post("/login", passport.authenticate("local", 
    {
        successRedirect: "/checkin",
        failureRedirect: "/login"
    }), function(req, res){
});

// logout route
router.get("/logout", function(req, res){
   req.logout();
   req.flash("success", "Obrigado, até mais!");
   res.redirect("/checkin");
});

// USER PERFIL
router.get("/users/:id", function(req, res){
   User.findById(req.params.id, function(err, foundUser){
        if(err){
            req.flash("error", "Algo deu errado.");
            res.redirect("/");
        }
        Checkin.find().where("author.id").equals(foundUser._id).exec(function(err, checkin){
            if(err){
            req.flash("error", "Something went wrong.");
            res.redirect("/");
            }
            res.render("users/show",{user: foundUser, checkin: checkin});
        });
        
   });
});

module.exports = router;