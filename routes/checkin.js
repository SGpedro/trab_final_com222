var express = require("express");
var router  = express.Router();
var Checkin = require("../models/checkin");
var Cerveja = require("../models/cerveja");
var middleware = require("../middleware");
var multer = require('multer');
var storage = multer.diskStorage({
  filename: function(req, file, callback) {
    callback(null, Date.now() + file.originalname);
  }
});
var imageFilter = function (req, file, cb) {
    // accept image files only
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/i)) {
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
};
var upload = multer({ storage: storage, fileFilter: imageFilter})

var cloudinary = require('cloudinary');
cloudinary.config({ 
  cloud_name: 'sgpedro', 
  api_key: '652958996313137', 
  api_secret: '97sTaRJQLgqmc0eE7Hj4YBrtBWw'
}); 


//INDEX - mostra todos os checkins e faz a pesquisa
router.get("/", function(req, res){
    if(req.query.search){
        // acha um post que bate com a search
        escapeRegex(req.query.search);
        const regex = new RegExp(escapeRegex(req.query.search), 'gi');
        Checkin.find({name: regex}, function(err, allCheckins){
        if(err){
           console.log(err);
        } else {
            if(allCheckins.length < 1){
                req.flash("error", "Nenhum Checkin com esse nome.");
                res.redirect("back");
            } else{
                res.render("checkin/index",{checkin: allCheckins, page: 'checkin'});    
            }
        }
    });
    }else{
    //mostra todos os posts feitos
    Checkin.find({}, function(err, allCheckins){
       if(err){
           console.log(err);
       } else {
          res.render("checkin/index",{checkin: allCheckins, page: 'checkin'});
       }
    });
    }
    
});

//CREATE - adiciona um novo checkin
router.post("/", middleware.isLoggedIn, upload.single('image'), function(req, res) {
    cloudinary.uploader.upload(req.file.path, function(result) {
      // add cloudinary url for the image to the campground object under image property
      req.body.checkin.image = result.secure_url;
      // autor no checkin
      req.body.checkin.author = {
        id: req.user._id,
        username: req.user.username
      }
      Checkin.create(req.body.checkin, function(err, checkin) {
        if (err) {
          req.flash('error', err.message);
          return res.redirect('back');
        }
        res.redirect('/checkin/' + checkin.id);
      });
    });
});

/*router.get("/new_cerveja", function(req, res){
    res.render("checkin/new_cerveja");
});

router.post("/new_cerveja", function(req, res){
    Cerveja.create(req.body.cerveja, function(err, cerveja) {
                        if (err) {
                            req.flash('error', err.message);
                            return res.redirect('back');
                        }
                        res.redirect('/checkin');
    });
});*/



//NEW - formulario para criar novo checkin
router.get("/new", middleware.isLoggedIn, function(req, res){
   res.render("checkin/new"); 
});

// SHOW - mostra com mais detalhes o checkin
router.get("/:id", function(req, res){
    //acha o checkin com o ID
    Checkin.findById(req.params.id).populate("comments").exec(function(err, foundCheckin){
        if(err){
            console.log(err);
        } else {
            //render show template with that campground
            res.render("checkin/show", {checkin: foundCheckin});
        }
    });
});

// EDIT CHECKIN ROUTE
router.get("/:id/edit", middleware.checkCheckinOwnership, function(req, res){
    Checkin.findById(req.params.id, function(err, foundCheckin){
        res.render("checkin/edit", {checkin: foundCheckin});
    });
});

// UPDATE CHECKIN ROUTE
router.put("/:id",middleware.checkCheckinOwnership, function(req, res){
    // find and update the correct campground
    Checkin.findByIdAndUpdate(req.params.id, req.body.checkin, function(err, updatedChekin){
       if(err){
           res.redirect("/checkin");
       } else {
           //redirect somewhere(show page)
           res.redirect("/checkin/" + req.params.id);
       }
    });
});

// DESTROY CHECKIN ROUTE
router.delete("/:id",middleware.checkCheckinOwnership, function(req, res){
   Checkin.findByIdAndRemove(req.params.id, function(err){
      if(err){
          res.redirect("/checkin");
      } else {
          res.redirect("/checkin");
      }
   });
});


function escapeRegex(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
};


module.exports = router;